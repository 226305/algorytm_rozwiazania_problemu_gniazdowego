#include <iostream>
#include <fstream>
#include <queue>


using namespace std;





int main()
{
    ifstream plik;
	string zmienna;

	int n,m,i,j;

	cout << "Podaj nazwe pliku wejsciowego:  ";
	cin >> zmienna;
	cout << endl;

	/* otwieramy plik */
	plik.open(zmienna);

	/* sprawdzamy czy plik sie otworzyl */
	if (!plik.good() == true)
	{
		cout << "Nie mozna otworzyc pliku\n";
		return -1;
	}

	/* pobieramy liczbe zadan */
	plik >> n;
	n+=1;
	int *T  =  new int[n];
	int *A  =  new int[n];
    int *P  =  new int[n];
    int *pi =  new int[n];

    /* Pobieram poprzednikow technologicznych */
    T[0]=0;
    for(i=1;i<n;++i)
        plik>>T[i];
    /* Pobieram poprzednikow maszynowych */
    A[0]=0;
    for(i=1;i<n;++i)
        plik>>A[i];
    /* Pobieram czasy */
    for(i=1;i<n;++i)
        plik>>P[i];


	/* pobieramy liczbe maszyn */
    plik >> m;
    m+=1;

    i = 1;
    while(i<n)
    {
        plik>>pi[i];
        if(pi[i]==0)
            --i;
        ++i;
    }

    cout<<"      ";
    for(i=1;i<n;++i)
        cout<<i<<" ";
    cout<<endl;
    cout<<endl;

    cout<<"T[i]  ";
    for(i=1;i<n;++i)
        cout<<T[i]<<" ";
    cout<<endl;
    cout<<"A[i]  ";
    for(i=1;i<n;++i)
        cout<<A[i]<<" ";
    cout<<endl;
    cout<<"P[i]  ";
    for(i=1;i<n;++i)
        cout<<P[i]<<" ";
    cout<<endl;
    cout<<"pi[i] ";
    for(i=1;i<n;++i)
        cout<<pi[i]<<" ";
    cout<<endl;


    int *LP = new int[n];
    for(i=1;i<n;++i)
        LP[i]=0;

    for(i=1;i<n;++i)
    {
        LP[T[i]]+=1;
        LP[A[i]]+=1;
    }

    int **C = new int*[n+1];
    for(i=0;i<n+1;++i)
        C[i] = new int[2];

    for(i=0;i<n+1;++i)
    {
        C[i][0]=0;
        C[i][1]=0;
    }

    queue<int> kolejka;
    for(i=0;i<n;++i)
        {
            if(LP[i]==0)
                kolejka.push(i);
            cout<<"LP: "<<LP[i]<<endl;
        }
    int *PT  =  new int[n];
	int *PA  =  new int[n];

	for(i=1;i<n;++i)
    {
        PT[i]=0;
        PA[i]=0;
    }
	for(i=1;i<n;++i)
    {
        PT[T[i]]=i;
        PA[A[i]]=i;
    }
    for(i=1;i<n;++i)
        cout<<i<<": "<<PT[i]<<" "<<PA[i]<<endl;

    while(!kolejka.empty())
    {

        i=kolejka.front();
        kolejka.pop();
        cout<<i<<endl;

        LP[T[i]]-=1;
        if(LP[T[i]]==0 && T[i]!=0)
            {
                kolejka.push(T[i]);
                //cout<<"T[i] = "<<T[i]<<"  LP[T[i]]= "<<LP[T[i]]<<endl;
            }
        LP[A[i]]-=1;
        if(LP[A[i]]==0 && A[i]!=0)
            {
                kolejka.push(A[i]);
                //cout<<"A[i] = "<<A[i]<<"LP[A[i]]= "<<LP[A[i]]<<endl;
            }


        C[i][0]=max(C[ PT[i] ][1],C[ PA[i] ][1]);
        C[i][1]=C[i][0]+P[i];
    }


    for(i=1;i<n;++i)
    {
        cout<<C[i][0]<<" "<<C[i][1]<<endl;
        if(C[i][1]>C[n][0])
            C[n][0]=C[i][1];
    }
    cout<<"Cmax= "<<C[n][0]<<endl;

}
